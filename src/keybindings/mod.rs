extern crate gtk;
extern crate gdk;
extern crate sourceview;
use std::sync::{Arc, Mutex};
use sourceview::{View};
use gtk::{TextViewExt,TextBufferExt};
use gtk::{FileChooserAction,FileChooserExt,ResponseType, DialogExt};
use std::ops::Deref;
use std::path::PathBuf;
use std::fs::File;
use std::io::prelude::*;
// use std::io::{Write,Read};

struct Keybind {
    modifer: gdk::ModifierType,
    key: char,
}

pub struct Actions {
    keybindings: Vec<Keybind>,
    main_file_path: Option<PathBuf>,
    main_text_view: Arc<Mutex<View>>,
}

impl Actions{
    pub fn new(text_view: Arc<Mutex<View>>) -> Actions{
        //@TODO(renzix) Read keybind from file
        let keybind = Keybind { modifer: gdk::ModifierType::CONTROL_MASK, key: 's'};
        let keybindings: Vec<Keybind> = vec![keybind];
        Actions {
            keybindings,
            main_file_path: Some(PathBuf::from(r"/home/genzix/Downloads/test.txt")),
            main_text_view: text_view,
        }
    }
    pub fn proc_input(&self, state: gdk::ModifierType ,key: Option<char>){
        /* @DEBUG(renzix)
        println!("State={:?}\tKey:{:?}",state,key); */
        if key!=None {
            let key = key.unwrap();
            if state==gdk::ModifierType::CONTROL_MASK {
                if key=='s' {
                    let buff = self.main_text_view.lock().unwrap().deref().get_buffer().unwrap();
                    Actions::save(self.main_file_path.as_ref(), buff.get_text(&buff.get_start_iter(), &buff.get_end_iter(), false).unwrap(),false);
                }else if key=='o' {
                    // Actions::open();
                }else if key=='c' {
                    // Actions::close();
                }
            }
        }
    }

    //@TODO(renzix): return bool
    fn save(path: Option<&PathBuf>,text: String, save_as: bool){
        let temppath = PathBuf::from("/home/genzix/Downloads/test.txt");
        let dialog = gtk::FileChooserDialog::with_buttons::<gtk::Window>(
            Some("Save File"),
            None,
            FileChooserAction::Save,
            &[("_Cancel", ResponseType::Cancel), ("_Save",ResponseType::Accept)]
        );
        let res = dialog.run();
        dialog.emit_close();
        if res == ResponseType::Accept.into() {
            println!("{:?}",dialog.get_filename());
        }else{
            return;
        }
        let path = match path{
            Some(t) => t,
            None => &temppath,
        };
        println!("Saving file\n-----------------------------\n{}\n-----------------------------",text);
        //TODO(renzix): Check if file exists save_as or file does not exist
        let mut file = File::create(path).ok().unwrap();
        file.write_all(format!("{}\n",text).as_ref()).ok();
        file.flush().ok();
    }
    fn open(path: &PathBuf) -> bool{
        //Change pathbuf and read to file
        let mut file = match File::open(path).ok() {
            Some(f) => f,
            None => return false,
        };
        return true;
    }
}

