extern crate gtk;
extern crate gdk;
extern crate sourceview;
use gtk::*;
use sourceview::*;
use std::process;
use std::ops::Deref;
use std::sync::{Arc,Mutex};
mod keybindings;

fn main() {
    if gtk::init().is_err(){
        eprintln!("Failed to init gtk");
        process::exit(1);
    }

    let app = App::new();
    app.window.connect_delete_event(move |_,_|{
        main_quit();
        Inhibit(false)
    });

    //Keybindings
    let view = app.content.view;
    app.window.connect_key_press_event(move |_win,key|{
        let state = key.get_state();
        let key_pressed = gdk::keyval_to_unicode(key.get_keyval());
        let act = keybindings::Actions::new(view.clone());
        act.proc_input(state,key_pressed);
        Inhibit(false)
    });

    app.window.show_all();

    gtk::main();
}

pub struct App {
    pub header: HeaderBar,
    pub window: Window,
    pub content: Content,
}

//@FEAT(renzix): Add changability to view?
pub struct Content {
    pub container: Box,
    pub view: Arc<Mutex<View>>,
    // pub run_box: Box,
    // pub run_text: TextView,
}

impl App {
    fn new() -> App{
        let header = HeaderBar::new();
        let window = Window::new(WindowType::Toplevel);
        header.set_title("TEXT");
        header.set_show_close_button(true);
        let content = Content::new();
        window.set_titlebar(&header);
        window.set_title("Redditor");
        Window::set_default_icon_name("iconname");
        window.set_default_size(1200,800);
        window.add(&content.container);
        // window.add(&content.run_box);
        App {header,window,content}
    }
}

impl Content {
    fn new() -> Content {
        //@TODO(renzix): Fix runbox
        // let run_box = Box::new( Orientation::Vertical,0);
        // let run_text = TextView::new();
        // run_text.get_buffer().unwrap().set_text("Run box");
        // run_box.add(&run_text);
        let container = Box::new(Orientation::Vertical,0);
        //THIS IS A DEFAULT CONFIGURATION
        let buff  = Buffer::new(None);
        let view  = View::new_with_buffer(&buff);
        view.set_show_line_numbers(true);
        view.set_tab_width(2);
        view.set_auto_indent(true);
        view.set_insert_spaces_instead_of_tabs(true);
        view.set_monospace(true);
        //@LOOK(renzix): Changable???
        let style = view.get_style_context().unwrap();
        let css = CssProvider::new();
        css.load_from_path("style/default.css").ok();
        style.add_provider(&css, STYLE_PROVIDER_PRIORITY_APPLICATION as u32);
        LanguageManager::new().get_language("rust").map(|rust| buff.set_language(&rust));
        let view = Arc::new(Mutex::new(view));
        container.pack_start(view.lock().unwrap().deref(),true,true,1);
        Content {container, view}
    }
}

